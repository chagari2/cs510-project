from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import MultifieldParser, OrGroup, QueryParser
from whoosh import scoring
from joblib import load
from label_topic import TopicLabels
from collections import defaultdict

class Myclass:
    def __init__(self):
        self.abBm25 = 0.0
        self.introBm25 = 0.0
        self.tiBm25 = 0.0
        self.abTf = 0.0
        self.introTf = 0.0
        self.tiTf = 0.0
        self.abPl2 = 0.0
        self.introPl2 = 0.0
        self.tiPl2 = 0.0
        self.abDf = 0.0
        self.introDf = 0.0
        self.tiDf = 0.0
        self.abstarct = ""
        self.introduction = ""
        self.title = ""
        self.paper = ""
        self.neuralScore = 0.0

class Search:
   
    def getResultDocs(self, query):
        docs = []
        indexDir = open_dir("index/")
        og = OrGroup.factory(0.9)
        indexSearcher = indexDir.searcher()

        # Parser for multiple fields
        abQueryParser = QueryParser("abstract", indexSearcher.schema, group=og)
        abQueryObject = abQueryParser.parse(query)
        introQueryParser = QueryParser("introduction", indexSearcher.schema, group=og)
        introQueryObject = introQueryParser.parse(query)
        tiQueryParser = QueryParser("title", indexSearcher.schema, group=og)
        tiQueryObject = tiQueryParser.parse(query)

        # Extracting features
        features = defaultdict()
        abResults = indexSearcher.search(abQueryObject, limit = 300)
        introResults = indexSearcher.search(introQueryObject, limit = 300)
        tiResults = indexSearcher.search(tiQueryObject, limit = 300)
        for result in abResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].abBm25 = result.score
            else:
                temp = Myclass()
                temp.abBm25 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp
        
        for result in introResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].introBm25 = result.score
            else:
                temp = Myclass()
                temp.pabm25 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        for result in tiResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].tiBm25 = result.score
            else:
                temp = Myclass()
                temp.tiBm25 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp
        
        
        w = scoring.TF_IDF()
        idfIndexSearcher = indexDir.searcher(weighting=w)
        abIdfResults = idfIndexSearcher.search(abQueryObject, limit = 300)
        introIdfResults = idfIndexSearcher.search(introQueryObject, limit = 300)
        tiIdfResults = idfIndexSearcher.search(tiQueryObject, limit = 300)
        for result in abIdfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].abTf = result.score
            else:
                temp = Myclass()
                temp.abTf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp
        
        for result in introIdfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].introTf = result.score
            else:
                temp = Myclass()
                temp.introTf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        for result in tiIdfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].tiTf = result.score
            else:
                temp = Myclass()
                temp.tiTf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        w = scoring.PL2()
        plIndexSearcher = indexDir.searcher(weighting=w)
        abPlResults = plIndexSearcher.search(abQueryObject, limit = 300)
        introPlResults = plIndexSearcher.search(introQueryObject, limit = 300)
        tiPlResults = plIndexSearcher.search(tiQueryObject, limit = 300)
        for result in abPlResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].abPl2 = result.score
            else:
                temp = Myclass()
                temp.abPl2 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp
        
        for result in introPlResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].introPl2 = result.score
            else:
                temp = Myclass()
                temp.introPl2 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        for result in tiPlResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].tiPl2 = result.score
            else:
                temp = Myclass()
                temp.tiPl2 = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp   

        w = scoring.DFree()
        dfIndexSearcher = indexDir.searcher(weighting=w)
        abDfResults = dfIndexSearcher.search(abQueryObject, limit = 300)
        introDfResults = dfIndexSearcher.search(introQueryObject, limit = 300)
        tiDfResults = dfIndexSearcher.search(tiQueryObject, limit = 300)
        for result in abDfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].abDf = result.score
            else:
                temp = Myclass()
                temp.abDf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp
        
        for result in introDfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].introDf = result.score
            else:
                temp = Myclass()
                temp.introDf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        for result in tiDfResults:
            paper = result["paper"]
            if (paper in features):
                features[paper].tiDf = result.score
            else:
                temp = Myclass()
                temp.tiDf = result.score
                temp.abstarct = result["abstract"]
                temp.title = result["title"]
                temp.introduction = result["introduction"]
                features[paper] = temp

        model = load('plsaNeuralNetModel.model')
        for key, val in features.items():
            ab = set(val.abstarct.split())
            intro = set(val.introduction.split())
            ti = set(val.title.split())
            q = set(query.split())
            val.neuralScore = model.predict([[val.abBm25, val.introBm25, val.tiBm25, \
                                            val.abTf, val.introTf, val.tiTf, \
                                            val.abPl2, val.introPl2, val.tiPl2, \
                                            val.abDf, val.introDf, val.tiDf, \
                                            len(ab), len(intro), len(ti), len(q), \
                                            len(q.intersection(ab)), len(q.intersection(intro)), len(q.intersection(ti))]])[0]
        
        results = dict(sorted(features.items(), key=lambda x: x[1].neuralScore, reverse=True)[:100])
        
        searchResults = list()
        for key, val in results.items():
            contents = dict()
            contents["paper"] = key
            contents["abstract"] = val.abstarct
            
            docs.append(val.abstarct)
            contents["title"] = val.title
            contents["introduction"] = val.introduction
            contents["topics"] = ""
            searchResults.append(contents)

        if len(searchResults) == 0:
            return searchResults

        doc_topics, labels = TopicLabels.get_topic_labels(docs,
                                n_topics=10,
                                n_top_words=20,
                                preprocessing_steps=['tag'],
                                n_cand_labels=100,
                                label_min_df=5,
                                label_tags=['NN,NN', 'JJ,NN'],
                                n_labels=10,
                                lda_random_state=12345,
                                lda_n_iter=400)
        
        topic_labels = dict()
        for key, val in doc_topics.items():
            for i, label in enumerate(labels):               
                if i == val:
                    topic_labels[key] = label

        for i, result in enumerate(searchResults):
            # TO-DO: format the output, remove stemming
            result["topics"] = str(topic_labels[i])

        return searchResults

