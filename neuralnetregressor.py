from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn import preprocessing as pre
from math import sin
import numpy as np
import csv
from collections import defaultdict

def relevanceScore(intercept, coefs, scores):
    relScore = intercept
    for index, score in enumerate(scores):
        relScore += (float(score) * coefs[index])
    return relScore

trainData = list()
f = open("./supervisedTrain.txt", "r")
for line in f:
    words = line.split(",")
    trainData.append(words)

trainData = np.array(trainData)
trainRel = np.array(trainData[:,0], dtype='float')
trainFeatures = np.array(trainData[:,1:-1], dtype='float')

scaler = pre.StandardScaler()
trainFeaturesScaled = scaler.fit_transform(trainFeatures)

testData = list()
tf = open("./neuralNetFeaturesTest.txt", "r")
for line in tf:
    words = line.split(",")
    testData.append(words)

# Train model....Got good results for 3 hidden layers with regression data
mlp = MLPRegressor(hidden_layer_sizes=(3, 3),
                    activation='tanh',
                    solver='adam',
                    learning_rate='invscaling',
                    max_iter=1000,
                    learning_rate_init=0.001,
                    alpha=0.001,
                    random_state=0,
                    shuffle=True)
mlp.fit(trainFeaturesScaled, trainRel)

testData = np.array(testData)
testDataQid = testData[:,0]
testDataDocNo = testData[:,-1]
testDataFeatures = np.array(testData[:,1:-1], dtype='float')
testDataFeaturesScaled = scaler.fit_transform(testDataFeatures)
testDataRel = mlp.predict(testDataFeaturesScaled)


testDict = defaultdict(list)
for index, rel in enumerate(testDataRel):
    temp = [rel, testDataDocNo[index]]
    testDict[testDataQid[index]].append(temp)

tf = open("./NeuralNetRegressionFeaturesTrainResults.txt", "w")
finalDict = defaultdict(list)
for key, value in testDict.items():
    value.sort(key=lambda x:x[0], reverse=True)
    finalDict[key] = value[:100]

for key, value in finalDict.items():
    for v in value: 
        tf.write(key + "\t" + v[1][:-1] + "\t" + str(v[0]) + "\n" )

# TODO Add evaluation methods to be invoked on test data set results.
from joblib import dump, load
dump(mlp, "plsaNeuralNetModel.model")