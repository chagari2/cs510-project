import json
import xml.etree.ElementTree as ET
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import MultifieldParser, OrGroup, QueryParser
from whoosh.scoring import BaseScorer
from whoosh import scoring
from collections import defaultdict

class Myclass:
    def __init__(self):
        self.rel = 0
        self.kpbm25 = 0.0
        self.pabm25 = 0.0
        self.tibm25 = 0.0
        self.kptf = 0.0
        self.patf = 0.0
        self.titf = 0.0
        self.kppl2 = 0.0
        self.papl2 = 0.0
        self.tipl2 = 0.0
        self.kpdf = 0.0
        self.padf = 0.0
        self.tidf = 0.0
        self.references = 0
        self.citatedBy = 0
        self.citations = 0


count = 0

rf = open("./supervisedTrain.txt", "w")
f = open("./train_queries.json", encoding = "utf-8")
relevance = open("./train_queries_qrel", encoding = "utf-8")
train_queries = defaultdict(dict)
for line in relevance:
    words = line.split()
    train_queries[words[0]][words[1]] = words[2]


fileWrite = defaultdict(dict)
queries = defaultdict(str)
for line in f:
        document = json.loads(line)
        query = document["query"]
        query = query.replace("#combine( ", '')
        query = query.replace(')', '')
        queries[document["qid"]] = query
        indexDir = open_dir("index/")
        og = OrGroup.factory(0.9)
        #QueryParser("content", schema)
        #queryParser = MultifieldParser(["keyPhrases", "paperAbstract", "title"], indexSearcher.schema, group=og)
        indexSearcher = indexDir.searcher()
        kpqueryParser = QueryParser("keyPhrases", indexSearcher.schema, group=og)
        kpqueryObject = kpqueryParser.parse(query)
        paqueryParser = QueryParser("paperAbstract", indexSearcher.schema, group=og)
        paqueryObject = paqueryParser.parse(query)
        tiqueryParser = QueryParser("title", indexSearcher.schema, group=og)
        tiqueryObject = tiqueryParser.parse(query)

        
        kpresults = indexSearcher.search(kpqueryObject, limit = None)
        paresults = indexSearcher.search(paqueryObject, limit = None)
        tiresults = indexSearcher.search(tiqueryObject, limit = None)
        for result in kpresults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].kpbm25 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.kpbm25 = result.score
                    fileWrite[str(count)][docNo] = temp
        
        for result in paresults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].pabm25 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.pabm25 = result.score
                    fileWrite[str(count)][docNo] = temp

        for result in tiresults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].tibm25 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.tibm25 = result.score
                    fileWrite[str(count)][docNo] = temp
        
        
        w = scoring.TF_IDF()
        idfIndexSearcher = indexDir.searcher(weighting=w)
        #idfResults = idfIndexSearcher.search(queryObject, limit = 8541)
        kpidfResults = idfIndexSearcher.search(kpqueryObject, limit = None)
        paidfResults = idfIndexSearcher.search(paqueryObject, limit = None)
        tiidfResults = idfIndexSearcher.search(tiqueryObject, limit = None)
        for result in kpidfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].kptf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.kptf = result.score
                    fileWrite[str(count)][docNo] = temp
        
        for result in paidfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].patf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.patf = result.score
                    fileWrite[str(count)][docNo] = temp

        for result in tiidfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].titf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.titf = result.score
                    fileWrite[str(count)][docNo] = temp

        w = scoring.PL2()
        plIndexSearcher = indexDir.searcher(weighting=w)
        #plResults = plIndexSearcher.search(queryObject, limit = 8541)
        kpplResults = plIndexSearcher.search(kpqueryObject, limit = None)
        paplResults = plIndexSearcher.search(paqueryObject, limit = None)
        tiplResults = plIndexSearcher.search(tiqueryObject, limit = None)

        for result in kpplResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].kppl2 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.kppl2 = result.score
                    fileWrite[str(count)][docNo] = temp
        
        for result in paplResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].papl2 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.papl2 = result.score
                    fileWrite[str(count)][docNo] = temp

        for result in tiplResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].tipl2 = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.tipl2 = result.score
                    fileWrite[str(count)][docNo] = temp 

        w = scoring.DFree()
        dfIndexSearcher = indexDir.searcher(weighting=w)
        #plResults = plIndexSearcher.search(queryObject, limit = 8541)
        kpdfResults = dfIndexSearcher.search(kpqueryObject, limit = None)
        padfResults = dfIndexSearcher.search(paqueryObject, limit = None)
        tidfResults = dfIndexSearcher.search(tiqueryObject, limit = None)

        for result in kpdfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].kpdf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.kpdf = result.score
                    fileWrite[str(count)][docNo] = temp
        
        for result in padfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].padf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.padf = result.score
                    fileWrite[str(count)][docNo] = temp

        for result in tidfResults:
            docNo = "".join(result["docno"])
            rel = train_queries[str(count)].get(docNo, -1)

            if rel != -1:
                if (docNo in fileWrite[str(count)]):
                    fileWrite[str(count)][docNo].tidf = result.score
                else:
                    temp = Myclass()
                    temp.rel = rel
                    temp.tidf = result.score
                    fileWrite[str(count)][docNo] = temp

        count += 1

def getJsonValue(jsonValue, key):
    try:
        if key != "docno":
            value = " ".join(jsonValue[key])
        else:
            value = jsonValue[key]
        return value
    except:
        return " "

docs = open("./docs.json", encoding="utf-8")
docReferences = defaultdict()
for line in docs:
    document = json.loads(line)
    kp = getJsonValue(document, "keyPhrases")
    pa = getJsonValue(document, "paperAbstract")
    ti = getJsonValue(document, "title")
    docNo = document["docno"]
    docReferences[docNo] = [kp, pa, ti]

for key, value in fileWrite.items():
    for k, v in value.items():
        kp = set(docReferences[k][0].split())
        pa = set(docReferences[k][1].split())
        ti = set(docReferences[k][2].split())
        q = set(queries[key].split())
        #rf.write(str(v[0]) + "\tqid:" + key + "\t1:" + str(v[1]) + "\t2:" + str(v[2]) + "\t3:" + str(v[3]) + "\t#docid = " + str(k) + "\n")
        rf.write(str(v.rel) + "," + str(v.kpbm25) + "," + str(v.pabm25) + "," + str(v.tibm25) + "," + \
            str(v.kptf) + "," + str(v.patf) + "," + str(v.titf)  + "," + \
            str(v.kppl2) + "," + str(v.papl2) + "," + str(v.tipl2) + "," + \
            str(v.kpdf) + "," + str(v.padf) + "," + str(v.tidf) + "," + \
            str(len(kp)) + "," + str(len(pa)) + "," + str(len(ti)) + "," + \
            str(len(q)) + "," + str(len(q.intersection(kp))) + "," + str(len(q.intersection(pa))) + "," + str(len(q.intersection(ti))) + "," + \
            str(k) + "\n")
f.close()
print(count)

