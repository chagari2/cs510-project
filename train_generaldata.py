import json
import xml.etree.ElementTree as ET
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import MultifieldParser, OrGroup, QueryParser
from whoosh.scoring import BaseScorer
from whoosh import scoring
from collections import defaultdict

class Myclass:
    def __init__(self):
        self.rel = 0
        self.kpbm25 = 0.0
        self.pabm25 = 0.0
        self.tibm25 = 0.0
        self.kptf = 0.0
        self.patf = 0.0
        self.titf = 0.0
        self.kppl2 = 0.0
        self.papl2 = 0.0
        self.tipl2 = 0.0
        self.kpdf = 0.0
        self.padf = 0.0
        self.tidf = 0.0

count = 0
tree = ET.parse("./trainqueries.xml")
root = tree.getroot()
rf = open("./supervisedTrain.txt", "w")
# f = open("./train_queries.json", encoding = "utf-8")
relevance = open("./train_qrel", encoding = "utf-8")
train_queries = defaultdict(dict)
for line in relevance:
    words = line.split()
    train_queries[words[0]][words[1]] = words[2]


for queries in root.findall('query'):
        for query in queries.findall('text'):
            fileWrite = defaultdict(dict)
            query = query.text
            query = query.replace("#combine( ", '')
            query = query.replace(')', '')
            indexDir = open_dir("index/")
            og = OrGroup.factory(0.9)
            
            w = scoring.BM25F(B=0.5, K1=1.2)
            indexSearcher = indexDir.searcher(weighting=w)
            kpqueryParser = QueryParser("docText", indexSearcher.schema, group=og)
            kpqueryObject = kpqueryParser.parse(query)
            kpresults = indexSearcher.search(kpqueryObject, limit = 500)
            for result in kpresults:
                docNo = "".join(result["docId"])
                rel = train_queries[str(count)].get(docNo, -1)

                if rel != -1:
                    if (docNo in fileWrite[str(count)]):
                        fileWrite[str(count)][docNo].kpbm25 = result.score
                    else:
                        temp = Myclass()
                        temp.rel = rel
                        temp.kpbm25 = result.score
                        fileWrite[str(count)][docNo] = temp
            
            
            w = scoring.TF_IDF()
            idfIndexSearcher = indexDir.searcher(weighting=w)
            #idfResults = idfIndexSearcher.search(queryObject, limit = 8541)
            kpidfResults = idfIndexSearcher.search(kpqueryObject, limit = 500)
            for result in kpidfResults:
                docNo = "".join(result["docId"])
                rel = train_queries[str(count)].get(docNo, -1)

                if rel != -1:
                    if (docNo in fileWrite[str(count)]):
                        fileWrite[str(count)][docNo].kptf = result.score
                    else:
                        temp = Myclass()
                        temp.rel = rel
                        temp.kptf = result.score
                        fileWrite[str(count)][docNo] = temp

            w = scoring.PL2()
            plIndexSearcher = indexDir.searcher(weighting=w)
            #plResults = plIndexSearcher.search(queryObject, limit = 8541)
            kpplResults = plIndexSearcher.search(kpqueryObject, limit = 500)
            for result in kpplResults:
                docNo = "".join(result["docId"])
                rel = train_queries[str(count)].get(docNo, -1)

                if rel != -1:
                    if (docNo in fileWrite[str(count)]):
                        fileWrite[str(count)][docNo].kppl2 = result.score
                    else:
                        temp = Myclass()
                        temp.rel = rel
                        temp.kppl2 = result.score
                        fileWrite[str(count)][docNo] = temp
            
            count += 1

            for key, value in fileWrite.items():
                for k, v in value.items():
                    #rf.write(str(v[0]) + "\tqid:" + key + "\t1:" + str(v[1]) + "\t2:" + str(v[2]) + "\t3:" + str(v[3]) + "\t#docid = " + str(k) + "\n")
                    rf.write(str(v.rel) + "," + str(v.kpbm25) + ","  + \
                        str(v.kptf) + "," +  \
                        str(v.kppl2) + "," + \
                        #str(v.kpdf) + ","  + \
                        str(k) + "\n")
# f.close()
print(count)

